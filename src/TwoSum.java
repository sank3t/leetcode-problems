import java.util.HashMap;

public class TwoSum {

	public int[] twoSum(int[] nums, int target) {
		for (int i = 0; i < nums.length; i++) {
			for (int j = i + 1; j < nums.length; j++) {
				if (target == (nums[i] + nums[j]))
					return new int[] { i, j };
			}
		}
		return new int[2];
	}
	
	public int[] twoSumHashTwoPass(int[] nums, int target)
	{
		HashMap<Integer, Integer> trackingMap= new HashMap<Integer, Integer>();
		
		for(int i=0; i<nums.length; i++)
		{
			trackingMap.put(nums[i],i);
		}
		
		for (int i=0; i<nums.length; i++)
		{
			int complement= target-nums[i];
			if(trackingMap.containsKey(complement) && trackingMap.get(complement)!=i)
			{
				return new int[] { i, trackingMap.get(complement) };
			}
		}
		
		return new int[2];
	}

	public int[] twoSumHashOnePass(int[] nums, int target)
	{
		HashMap<Integer, Integer> trackingMap= new HashMap<Integer, Integer>();
		
		for(int i=0; i<nums.length; i++)
		{
			int complement = target-nums[i];
			
			if(trackingMap.containsKey(complement))
			{
				return new int[] {trackingMap.get(complement), i};
			}
			trackingMap.put(nums[i], i);
		}
		return new int[2];
	}
	
	public static void main(String[] args) {
		int[] sample= new int[] {3,2,4};
		int target = 6;
		
		TwoSum ts= new TwoSum();
		int[] result= new int[] {};
		result= ts.twoSumHashOnePass(sample, target);
		for(int i =0; i<result.length; i++) {
		System.out.print(result[i]+" ");
		}
		
	}

}
